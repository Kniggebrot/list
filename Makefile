CC = gcc
LD = g++

INCLUDES = include lib
IFLAG 	 = $(patsubst %,-I%,$(INCLUDES))
LFLAGS   = -Wall -g
LSTATIC  = -static-libstdc++ -static-libgcc
CFLAGS   = $(LFLAGS) $(IFLAG)

SRCDIR = src
SRCS = $(wildcard $(SRCDIR)/*.cpp)
LIBDIR = lib
LIBS = $(wildcard $(LIBDIR)/*.cpp)

OBJDIR = build
OBJS = $(SRCS:$(SRCDIR)/%.cpp=$(OBJDIR)/%.o)
LIBOBJS = $(LIBS:$(LIBDIR)/%.cpp=$(OBJDIR)/%.o)

LIBDEPS = lib/list_doubly.hpp

default: build

build: $(OBJS) $(LIBOBJS)
	$(LD) $(LFLAGS) -o Lager $(OBJS) $(LIBOBJS)

builddir:
	@mkdir -p build

static: $(OBJS) $(LIBOBJS)
	$(LD) $(LFLAGS) $(LSTATIC) -o Lager $(OBJS) $(LIBOBJS)

$(OBJS): $(OBJDIR)/%.o: $(SRCDIR)/%.cpp | builddir
	$(CC) $(CFLAGS) -o $@ -c $<

$(LIBOBJS): $(OBJDIR)/%.o: $(LIBDIR)/%.cpp $(LIBDEPS) | builddir
	$(CC) $(CFLAGS) -o $@ -c $<

# list_doubly.o: lib/list_doubly.cpp
#	$(CC) $(CFLAGS) -o "$(OBJDIR)/list_doubly.o" -c "lib/list_doubly.cpp"

# Lager.o: src/Lager.cpp
#	$(CC) $(CFLAGS) -o "build/Lager.o" -c "src/Lager.cpp"

.PHONY: clean
clean: | builddir
	rm -f build/*o
	rmdir build

.PHONY: remove
remove:
	rm -f Lager.exe
