#include "list_doubly.hpp"
#include <cstring>
#include <fstream>
#include <sstream>

#define COMPONENTMAX 3
// fit to max component number

using namespace std;

list_doubly *read_file(string filepath)
{
    list_doubly *object = nullptr;
    string line;
    string attribute;

    int component = 0;
    const int component_max = COMPONENTMAX;

    string inname;
    float invalue;
    int inlot;

    cout << "Oeffne " << filepath << "..." << endl;
    ifstream import (filepath);

    if (import.is_open())
    {
        while (getline(import,line))
        {
            stringstream input(line);
            for (component = 0; component < component_max; component++)
            {
                getline(input,attribute,',');
                switch(component)
                {
                    case 0: // name
                        inname = attribute;
                        break;
                    case 1: // value
                        invalue = stof(attribute);
                        break;
                    case 2: // lot_number
                        inlot = stoi(attribute);
                        break;
                }
            }
            object = new list_doubly(inname,invalue,inlot);
            // input >> inname >> invalue >> inlot;
        }
        cout << "Import done." << endl;
        import.close();
        return object;
    }
    else
    {
        cout << "Unable to open file." << endl;
        throw filepath;
    }

}

void write_file(string filepath, list_doubly *object)
{
    if (object == nullptr) throw "No objects in list.";

    const int component_max = COMPONENTMAX;
    
    char direction = object->print_direction(false);
    list_doubly *start = object->get_start(direction);

    while (start != nullptr)
    {
        start = start->export_object(filepath,direction,component_max);
    }

    cout << "Export to " << filepath << " done." << endl;    
}

list_doubly *read_args(int argc, char *argv[])
{
    int b=0;

    int section[5] = {0,0,0,0,0};
    int read;

    list_doubly *object = nullptr;

    for (int a = 0; a < argc; a++)      // extract "commands"
    {
        if (argv[a][0] == '-')
        {
            section[b] = a;
            b++;
        }
    }
    for (int c=0; section[c] != 0; c++)
    {
        if ( strcmp(argv[section[c]], "-h") == 0|| strcmp(argv[section[c]], "--help") == 0) // Help: Display possible options 
        {
            cout << "Possible arguments: " << endl;
            cout << "-i / --import [File1 File2 ...]:\n\t Imports files separated by whitespace in their order on startup." << endl;
            cout << "-f / --find (key) (value):\n\t Find and select item with \"value\" for \"key\" (name, value, lot_number) on startup." << endl;
            cout << "-d / --delete:\n\t Delete the currently selected item. ONLY USABLE AFTER -f / --find." << endl;
            cout << "-s / --sort (key):\n\t Sort the list by \"key\" on startup." << endl;
            cout << "-e / --export (filepath):\n\t Export the list to \"filepath\" on startup and quit.\n\t WILL OVERWRITE existing file." << endl;
            cout << "-h / --help:\n\t Displays this message." << endl;
            throw 69; // so program will be exited
        }
        else if ( strcmp(argv[section[c]], "-i") == 0 || strcmp(argv[section[c]], "--import") == 0 ) // Import: Can take multiple arguments
        {
            for (read = section[c] + 1 ; read < argc ; read++) // Determine, which strings after "-i" are Filenames: either until the next "command" or until the last arg
            {
                try{        // To avoid a nullptr if one file has already been successfully imported
                    object = read_file(argv[read]);
                }
                catch (string s){
                    cout << "Problem with file " << s << ". Contents not imported." << endl;
                }
                if (section[c+1] != 0)
                {
                    if (read == section[c+1] - 1) break;    // Break one item before the next command
                }
            }
        } // IMPORT
        else if ( strcmp(argv[section[c]], "-f") == 0 || strcmp(argv[section[c]], "--find") == 0 ) // Find: Takes 2 arguments
        {
            string key = argv[section[c]+1];
            string value_buffer = argv[section[c]+2];

            if (object != nullptr)   // Will only execute search if at least one file has already been imported
            {
                try
                {
                    float value = stof(value_buffer);
                    try{
                        object = object->find_item(key, value);
                    }
                    catch (string s){
                    cout << "Error: No item with " << s << " as key or value found." << endl;
                    }
                    break;
                }
                catch (...){} // Might be a string then            
                string value = value_buffer;
                try{
                        object = object->find_item(key, value);
                }
                catch (string s){
                    cout << "Error: No item with " << s << " as key or value found." << endl;
                }
                // break;
            }
        } // FIND
        else if ( strcmp(argv[section[c]], "-d") == 0 || strcmp(argv[section[c]], "--delete") == 0 ) // Delete: Takes no argument
        {
            const char *prev_comm = argv[section[c-1]];

            if ( (object != nullptr) && ( strcmp(prev_comm, "-f") == 0 || strcmp(prev_comm, "--find") == 0 ) )   // Will only delete if an item exists and find has been called right before
            {
                cout << "Entferne:" << endl;
                object->print_item();
                object = object->remove_item();
            }
        } // DELETE
        else if ( strcmp(argv[section[c]], "-s") == 0 || strcmp(argv[section[c]], "--sort") == 0 ) // Sort: Takes 1 argument
        {
            string key = argv[section[c]+1];

            if (object != nullptr)   // Will only sort list if at least one file has already been imported
            {
                try
                {
                    object->sort_list(key,true);
                }
                catch(...)
                {
                    cout << "Sorting failed. Invalid key maybe?" << endl;
                }
            }
        } // SORT
        else if ( strcmp(argv[section[c]], "-e") == 0 || strcmp(argv[section[c]], "--export") == 0 ) // Export: Takes 1 argument
        {
            string filepath = argv[section[c]+1];
            
            ofstream ofile (filepath, ios::trunc);
            ofile.close();

            if (object != nullptr)   // Will only sort list if at least one file has already been imported
            {
                try
                {
                    write_file(filepath,object);
                    throw 69;
                }
                catch(string s)
                {
                    cout << "Exporting to " << filepath << " failed." << endl;
                }
            }
        } // EXPORT
    }
    return object;
}

int main(int argc, char *argv[])
{
    // TODO: Read argument for reversed output (-r)
    
    char a,b,c;
    string key;
    string value_buffer;
    string filepath;
    fstream exist;
    list_doubly *object = nullptr;

    cout << "Willkommen im Lagerprogramm!" << endl;
    try
    {
        object = read_args(argc,argv);
    }
    catch (int h)
    {
        return EXIT_SUCCESS;
    }

    while (true) // Menu
    {
        bool exporter = true;
        cout << "Bitte waehlen sie einen Menueeintrag: " << endl;
        cout << "1 - Neuen Eintrag hinzufuegen\n"
                "2 - Eintraege aus Datei hinzufuegen\n"
                "3 - Liste ausgeben\n"
                "4 - Liste sortieren\n"
                "5 - Eintrag suchen, auswaehlen & ausgeben\n"
                "6 - Ausgewaehlten Eintrag ausgeben\n"
                "7 - Ausgewaehlten Eintrag loeschen\n"
                "8 - Eintraege in Datei exportieren\n"
                "9 - Programm beenden"
             << endl;

        a = getchar();
        cin.ignore(2,'\n');
        switch (a)
        {
        case '1':
            object = new list_doubly;
            break;
        case '2':
            cout << "Bitte geben Sie den Pfad zur Datei (inklusive Dateinamen) ein: " << endl;
            getline(cin, filepath);
            //cin.ignore(1,'\n');
            try{
                object = read_file(filepath);
            }
            catch(string s){
                cout << "Problem with file" << s << ". Contents not imported." << endl;
            }
            break;
        case '3':
            if (object != nullptr)
                object->print_list();
            else
                cout << "List is empty." << endl;
            break;
        case '4':
            cout << "Wonach soll sortiert werden? (name, value, lot_number)" << endl;
            getline(cin,key);
            object->sort_list(key,true);
            //object->print_list();
            break;
        case '5':
            //cin.ignore(2,'\n');
            cout << "Mit welchem Attribut wird gesucht? (name, value, lot_number)" << endl;
            getline(cin,key);
            cout << "Mit welchem Wert?" << endl;
            getline(cin, value_buffer);
            cout << "Suche..." << endl;
            try
            {
                int value = stoi(value_buffer);
                try{
                    object = object->find_item(key, value);
                }
                catch (string s){
                    cout << "Error: No item with " << s << " as key or value found." << endl;
                }
                break;
            }
            catch (...){} // Might be a float
            try
            {
                float value = stof(value_buffer);
                try{
                    object = object->find_item(key, value);
                }
                catch (string s){
                    cout << "Error: No item with " << s << " as key or value found." << endl;
                }
                break;
            }
            catch (...){} // Might be a string then
            {
                string value = value_buffer;
                try{
                    object = object->find_item(key, value);
                }
                catch (string s){
                    cout << "Error: No item with " << s << " as key or value found." << endl;
                }
                break;
            }
        case '6':
            object->print_item();
            break;
        case '7':
            //cin.ignore(2,'\n');
            Deleting:
            cout << "Sind Sie sicher, dass Sie das aktuell ausgewaehlte Element loeschen wollen? (j/n)" << endl;
            object->print_item();
            b = getchar();
            cin.ignore(2,'\n');
            switch(b)
            {
                case 'y':
                case 'Y':
                case 'j':
                case 'J':
                    cout << "Loesche Item..." << endl;            
                    object = object->remove_item();
                    break;
                case 'n':
                case 'N':
                    cout << "Loeschvorgang abgebrochen." << endl;
                    break;
                default:
                    cout << "Bitte 'j' für Ja oder 'n' für Nein eingeben." << endl;
                    goto Deleting;
            }
            break;
        case '8':
            cout << "Bitte geben Sie den Pfad zur Zieldatei (inklusive Dateinamen) ein: " << endl;
            getline(cin, filepath);
            //cin.ignore(1,'\n');
            exist.open(filepath, ios::in);      // Test for existing file; ask for overwrite or append
            if (exist)
            {
                cout << filepath << " already exists. Do you want to overwrite or append? (o/a) " << endl;
                c = getchar();
                cin.ignore(2,'\n');
                switch(c)
                {
                    case 'O':
                    case 'o':
                        cout << "Overwriting " << filepath << "..." << endl;
                        exist.close();
                        exist.open(filepath, ios::out | ios::trunc);
                        exist.close();
                        break;
                    case 'A':
                    case 'a':
                        cout << "Appending " << filepath << "..." << endl;
                        exist.close();
                        break;
                    default:
                        cout << "Ungueltige Option. Export abgebrochen." << endl;
                        exporter = false;
                        break;
                }
            }
            if (!exporter) break; // Break this case on failed option
            try{
                write_file(filepath,object);
            }
            catch(string s){
                cout << "Liste konnte nicht nach " << s << " exportiert werden." << endl;
            }
            break;
        case '9':
            cout << "Vielen Dank fuer die Nutzung des Lagerprogrammes!" << endl;
            return EXIT_SUCCESS;
        default:
            cout << "Ungueltige Option. Bitte valide Option eingeben." << endl;
        }
    }
    /*    // cin.get();
    new list_doubly("Gold",900.123,7);
    cout << "Done!" << endl;
    new list_doubly("Silver",564.4541,2);
    new list_doubly("Copper",69.9,420);
    new list_doubly();
   cout << firstobj << " , " << lastobj << endl;
    firstobj->print_list();
    cout << firstobj << " , " << lastobj << endl;
    cin.get(); */

    return 0;
}