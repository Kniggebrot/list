#!/usr/bin/env bash
echo "Testing..."
./Lager -i example.csv -s value -f name Gold -d -e out.csv
comm -3 out.csv target.csv > error.csv
if [ -s error.csv ]; then
    echo "Error: error.csv is not empty, thus some commands must have failed. Please check error.csv."
    cat error.csv
    exit 123
else
    echo "Test successful: Empty error.csv."
fi
echo "Testing done."
